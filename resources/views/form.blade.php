@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<h4>{{$action}} Book</h4>
			<form method="POST" action="{{($action == 'Edit') ? route('books.update',['id'=>$book->id]):route('books.store')}}">
				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="title">Title <span class="text-danger">*</span></label>
							<input type="text" class="form-control" name="title" value="{{(Request::old('title')) ?? @$book->title }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="author">Author <span class="text-danger">*</span></label>
							<input type="text" class="form-control" name="author" value="{{(Request::old('author')) ?? @$book->author }}">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="genre">Genre <span class="text-danger">*</span></label>
							<input type="text" class="form-control" name="genre" value="{{(Request::old('genre')) ?? @$book->genre }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="publish_date">Publish Date <span class="text-danger">*</span></label>
							<input type="date" class="form-control" name="publish_date" min="1" value="{{(Request::old('publish_date')) ?? @$book->publish_date }}" max="{{date('Y-m-d')}}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="price">Price <span class="text-danger">*</span></label>
							<input type="number" class="form-control" name="price" value="{{(Request::old('price')) ?? @$book->price }}" step="any" >
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="description">Description <span class="text-danger">*</span></label>
							<textarea name="description" class="form-control" rows="4">{{(Request::old('description')) ?? @$book->description }}</textarea>
						</div>
					</div>
				</div>
				@csrf
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				@if (session('success'))
				    <div class="alert alert-success">
				       {{session('success')}}
				    </div>
				@endif
				<button type="submit" class="btn btn-md btn-primary">Save</button>
				<a type="button" class="btn btn-md btn-secondary" href="{{route('home')}}">Close</a>
			</form>
		</div>
	</div>
</div>
@endsection