@extends('layouts.app')
@section('css')
 <!-- Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 84px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 30px;
    }
</style>
@endsection
@section('content')
<div class="flex-center position-ref full-height">
   
    <div class="content">
        <div class="title m-b-md">
            Books
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-6">
                         @if(count($books))
                            {{$books->links()}}
                        @endif
                    </div>
                    <div class="col-md-6 text-right">
                         @auth
                        <a type="button" class="btn btn-md btn-success" href="{{route('books.create')}}">Add</a>
                        @endauth
                    </div>
                </div>
               
                <table class="table table-bordered table-hover text-center">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Description</th>
                            <th>Publish Date</th>
                            <th>Price</th>
                            @auth
                            <th colspan="2"></th>
                            @endauth
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($books))
                            @foreach($books as $key => $row)
                                <tr>
                                    <td>{{$row->title}}</td>
                                    <td>{{$row->author}}</td>
                                    <td>{{$row->description}}</td>
                                    <td>{{date('F d, Y',strtotime($row->publish_date))}}</td>
                                    <td>{{$row->price}}</td>
                                    @auth
                                    <td>
                                        <a type="button" class="btn btn-md btn-warning" href="{{route('books.edit',['id'=>$row->id])}}">Edit</a>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-md btn-danger btn_delete_book" data-id="{{$row->id}}">Delete</buttton>
                                    </td>
                                    @endauth
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No books found.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $(function(){

        $(this).on('click', '.btn_delete_book', function() {
            var id = $(this).data('id');
           swal('Are you sure?','Book will be permanently deleted.','warning',{dangerMode:true,buttons:true})
           .then((isSubmit)=>{
                $.ajax({
                    url: "delete/"+id,
                    type: 'DELETE',
                    data: {_token:$('meta[name="csrf-token"]').attr('content')},
                })
                .done(function() {
                    swal('Success!','Book successfully deleted.','success');
                    window.location.reload();
                })
                .fail(function() {
                    console.log("error");
                });
                
           }); 
        });
    });
</script>
@endsection

