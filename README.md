
## Clone the project

- git clone https://ralfsab@bitbucket.org/ralfsab/book_collections.git

after cloning, go to the project folder

- cd book_collections

## Install the dependencies

- composer install

## Create a database

## Copy the .env.example file and rename it as .env

Set your database variables in your .env file

## Generate app key

- php artisan key:generate 

## Migrate the database migrations and run seeder

- php artisan migrate --seed

## Generate passport client

- php artisan passport:client

Which user ID should the client be assigned to?:
[press ENTER key]

What should we name the client?:
 user

Where should we redirect the request after authorization? [http://localhost/aut
h/callback]:
[press ENTER key]



## Generate passport keys

- php artisan passport:keys

next is.

- php artisan passport:client --personal

What should we name the personal access client? [Laravel Personal Access Client
]:
user


## Now run the project

- php artisan serve

