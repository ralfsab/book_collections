<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    } 

    protected function validateFormData($request)
    {
        $request->validate([
            'title'=>'required|max:255',
            'author'=>'required|max:255',
            'genre'=>'required|max:255',
            'publish_date'=>'required|date_format:"Y-m-d"|before:today',
            'price'=>'required|numeric|gte:0',
            'description'=>'required|max:255',
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $books = Book::orderBy('created_at','desc')->paginate(5);

        return view('home',compact('books'));
    }

    public function create()
    {
        return view('form',['action'=>'Add New']);
    }

    public function store(Request $request)
    {
        $this->validateFormData($request);

        $book = new Book;
        $book->title = $request->title;
        $book->author = $request->author;
        $book->genre = $request->genre;
        $book->publish_date = $request->publish_date;
        $book->price = $request->price;
        $book->description = $request->description;
        $book->save();

        return back()->withSuccess('Successfully Added!');

    }

    public function edit($id)
    {

        $book = Book::where('id',$id)->first();

        if(empty($book))
        {
            return redirect()->route('home');
        }

        $action = 'Edit';
        return view('form',compact('book','action'));
    }

    public function update(Request $request, $id)
    {
        $this->validateFormData($request);

        $book = Book::find($id);
        $book->title = $request->title;
        $book->author = $request->author;
        $book->genre = $request->genre;
        $book->publish_date = $request->publish_date;
        $book->price = $request->price;
        $book->description = $request->description;
        $book->save();

        return back()->withSuccess('Successfully Updated!');
    }

    public function delete($id)
    {
        Book::where('id',$id)->delete();

        return json_encode(['success'=>1]);
    }
}
