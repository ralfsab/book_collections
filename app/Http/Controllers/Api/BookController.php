<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Book;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    protected function convert2xml($data)
    {
        $domDocu = new \DomDocument('1.0','UTF-8');
        $domDocu->formatOutput = true;

        $books_xml = $domDocu->createElement('collection');
        $domDocu->appendChild($books_xml);

        foreach ($data as $key => $val) {

            $book_xml = $domDocu->createElement('book');
            $books_xml->appendChild($book_xml);

            foreach (json_decode($val) as $col => $value) {
                $book_xml->appendChild($domDocu->createElement("$col","$value"));
            }   
        }

        return $domDocu->saveXML();
    }

    public function validateFormData($form_data)
    {
        $validator = Validator::make($form_data,[
            'title'=>'required|max:255',
            'author'=>'required|max:255',
            'genre'=>'required|max:255',
            'publish_date'=>'required|date_format:"Y-m-d"|before:today',
            'price'=>'required|numeric|gte:0',
            'description'=>'required|max:255',
        ]);

        if ($validator->fails()) {
            return response(['errors'=>$validator->errors()],422);
        }
    }

    public function get(Request $request)
    {
    
        $books = Book::all();

        if($request->header('Accept') == 'application/xml' || ($request->type == 'xml' && $request->header('Accept') == 'application/xml') )
        {   
            $data_xml = $this->convert2xml($books); 

            return response($data_xml)->header('Content-type','text/xml');
        }

        return response($books,200);
    }

    public function store(Request $request)
    {
        $this->validateFormData($request->all());

        $book = new Book;
        $book->title = $request->title;
        $book->author = $request->author;
        $book->genre = $request->genre;
        $book->publish_date = $request->publish_date;
        $book->price = $request->price;
        $book->description = $request->description;
        $book->save();

        return response(['success'=>true], 200);
    }


    public function edit(Request $request,$id)
    {
        $book = Book::where('id', $id);

        if ($request->header('Accept') == 'application/xml' || ($request->type == 'xml' && $request->header('Accept') == 'application/xml') ) {

            $data_xml = $this->convert2xml($book->get());

            return response($data_xml)->header('Content-type','text/xml');
        }

        $book = $book->first();

        return response($book, 200);
    }

    public function update(Request $request, $id)
    {
        $this->validateFormData($request->all());
  
        $book = Book::find($id);
        $book->title = $request->title;
        $book->author = $request->author;
        $book->genre = $request->genre;
        $book->publish_date = $request->publish_date;
        $book->price = $request->price;
        $book->description = $request->description;
        $book->save();

        return response(['success'=>true],200);

    }

    public function delete($id)
    {
        Book::where('id',$id)->delete();

        return response(['success',true],200);
    }
}
