<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Hash;
use App\User;
class RegisterController extends Controller
{
    //
    public function register(Request $request)
    {
    	$validator = Validator::make($request->all(),[
    		'name' => 'required|max:255',
    		'email' => 'required|email|max:255|unique:users',
    		'password' => 'required|min:8|string|confirmed'
    	]);

    	if ($validator->fails()) {
    		return response(['errors'=>$validator->errors()], 422);
    	}

    	$request->merge(['password'=>Hash::make($request->password)]);

    	$user = new User;
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = $request->password;
    	$user->save();

    	return response(['message'=>'Successfully registered.']);
    	
    }
}
