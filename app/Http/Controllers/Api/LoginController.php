<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Hash;
use App\User;
class LoginController extends Controller
{
    //

    public function login(Request $request)
    {
    	$validator = Validator::make($request->all(),[
    		'email' => 'required|email',
    		'password' => 'required'
    	]);

    	if($validator->fails())
    	{
    		return response(['errors'=> $validator->errors()],422);
    	}

    	$user = User::where('email',$request->email)->first();

    	if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('auth_token')->accessToken;
                $response = ['token' => $token,'message'=> 'Successfully logged in'];
                return response($response, 200);
            } else {
                $response = ["message" => "Password mismatch"];
                return response($response, 422);
            }
        } else {
            $response = ["message" =>'User does not exist'];
            return response($response, 422);
        }
    }

    public function logout(Request $request)
    {
    	auth('api')->user()->token()->revoke();

    	return response(['message'=>'Successfully logged out'], 200);
    }
}
