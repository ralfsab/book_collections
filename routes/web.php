<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'BookController@index')->name('index');
Route::get('/home', 'BookController@index')->name('home');
Route::get('/create', 'BookController@create')->name('books.create');
Route::post('/store', 'BookController@store')->name('books.store');
Route::get('/edit/{id}', 'BookController@edit')->name('books.edit');
Route::post('/update/{id}', 'BookController@update')->name('books.update');
Route::delete('/delete/{id}', 'BookController@delete')->name('books.delete');
