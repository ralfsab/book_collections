<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/register','Api\RegisterController@register');
Route::post('/login','Api\LoginController@login');
Route::post('/logout','Api\LoginController@logout');

Route::get('/books/get','Api\BookController@get');
Route::post('/books/store','Api\BookController@store');
Route::get('/books/edit/{id}','Api\BookController@edit');
Route::post('/books/update/{id}','Api\BookController@update');
Route::delete('/books/delete/{id}','Api\BookController@delete');

